/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.google.messaging;

import java.util.HashMap;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.ActivityData;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleDialog;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class MessagingActivity extends Activity implements AppembleActivity {
	AppembleDialog mAppembleDialog = null;
	String sSystemDbName, sContentDbName;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

		Bundle targetParameterValueList = getIntent().getExtras();
		String sDisplayClassActivity = targetParameterValueList.getString("display_activity_class");
		sSystemDbName = targetParameterValueList.getString(Constants.STR_SYSTEM_DATABASE_NAME);
		sContentDbName = targetParameterValueList.getString(Constants.STR_CONTENT_DATABASE_NAME); 
		String sTarget = targetParameterValueList.getString(Constants.STR_SCREEN_NAME);
		if (null != sSystemDbName && null != sContentDbName && null != sTarget &&  
				null != sDisplayClassActivity && "dialog".equalsIgnoreCase(sDisplayClassActivity)) {
			mAppembleDialog = new AppembleDialog(this);
			if (false == mAppembleDialog.createLayout(sSystemDbName, sContentDbName, sTarget, targetParameterValueList))
				return;
			mAppembleDialog.setCloseParentActivity(true);			// finish() the activity... The dialog should call getContext(), typecase it to Activity and call Activtiy.finish()
			mAppembleDialog.show();
			mAppembleDialog.onResume();
		} else if (targetParameterValueList.getBoolean("display_message", false)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder
		    	.setMessage(targetParameterValueList.getString("message"))
		    	.setTitle(targetParameterValueList.getString("title"))
		    	.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) { //Stop the activity
		                finish();    
		            }	
		        })
				.create()
		    	.show();
		} else // just close this activity.
			finish();
        //---look up the notification manager service---
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);	 
        //---cancel the notification---
        nm.cancel(getIntent().getExtras().getInt(GenerateNotification.STR_NOTIFICATION_ID));        			
	}

	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public HashMap<Integer, Cursor> getCursors() {
		return null;
	}

	@Override
	public View getRootLayout() {
		return this.getRootLayout();
	}

	@Override
	public void setActivityCreateStatus(boolean bBoolean) {
	}

	@Override
	public boolean getActivityCreateStatus() {
		return false; // no need of this
	}

	@Override
	public boolean getNetworkAvailableFlag() {
		return false;
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
	}

	@SuppressLint("Override")
	@Override
	public boolean isDestroyed() {
		return false;
	}

	@Override
	public Vector<ControlModel> getAllControls() {
		return mAppembleDialog != null ? mAppembleDialog.getAllControls() : null;
	}

	@Override
	public ScreenModel getScreenModel() {
		return mAppembleDialog != null ? mAppembleDialog.getScreenModel() : null;
	}

	@Override
	public Bundle getTargetParameterValueList() {
		return mAppembleDialog != null ? mAppembleDialog.getTargetParameterValueList() : null;
	}

	@Override
	public Activity getMyParent() {
		return this.getParent();
	}
	
	@Override
	public void finish() {
		// Raise the event ON_NOTIFICATION_READ
		ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName, 
				Messaging.ON_NOTIFICATION_READ);
	    for (int i = 0; null != actions && i < actions.length; i++)
	    	Action.callAction(this, null, null, actions[i], getTargetParameterValueList());		
	}

	@Override
	public boolean getActivityRecreateStatus() {
		// TODO Auto-generated method stub
		return mAppembleDialog != null ? mAppembleDialog.getActivityRecreateStatus() : null;
	}

	@Override
	public ActivityData getActivityData() {
		// TODO Auto-generated method stub
		return mAppembleDialog != null ? mAppembleDialog.getActivityData() : null;
	}
}
