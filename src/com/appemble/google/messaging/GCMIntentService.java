/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.appemble.google.messaging;

import static com.appemble.google.messaging.CommonUtilities.displayMessage;
import android.content.Context;
import android.content.Intent;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.appemble.google.messaging.GenerateNotification;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {
	String sContentDbName = null;
	String sSystemDbName = null;
    
    public GCMIntentService() {
        super();
        LogManager.logDebug("SenderId: " + CommonUtilities.SENDER_ID);
    }

    @Override
	protected String[] getSenderIds(Context context) {
		if (null == Cache.context && false == Cache.initiate(context.getApplicationContext(), false)) {
			LogManager.logError("Unable to initiate the cache");
			return null;
		}
    	CommonUtilities.REGISTER_SERVER_URL = Cache.bootstrap.getValue("REGISTER_SERVER_URL");
    	CommonUtilities.DEREGISTER_SERVER_URL = Cache.bootstrap.getValue("DEREGISTER_SERVER_URL");
    	CommonUtilities.SENDER_ID = Cache.bootstrap.getValue("SENDER_ID");
    	sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
    	sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
		String[] senderIds = new String[] { CommonUtilities.SENDER_ID };
		return senderIds;
    }
    
    @Override
    protected void onRegistered(Context context, String registrationId) {
        LogManager.logInfo("Device registered: regId = " + registrationId);
        displayMessage(context, getString(R.string.gcm_registered));
        if (ServerUtilities.register(context, Messaging.getAttributes(), 
        		registrationId) && null != sSystemDbName) {
        	// raise the event ON_NOTIFICATION_REGISTERED TODO
        }
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        LogManager.logInfo("Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            if (ServerUtilities.unregister(context, registrationId)) {
            	// raise the event ON_NOTIFICATION_DEREGISTERED Is this needed?
            }
        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            LogManager.logInfo("Ignoring unregister callback");
        }
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        LogManager.logInfo("Received message");
        String message = getString(R.string.gcm_message);
        displayMessage(context, message);
        // notifies user
        GenerateNotification.generateNotification(context, intent, null, sSystemDbName, sContentDbName);
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        LogManager.logInfo("Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        GenerateNotification.generateNotification(context, null, message, sSystemDbName, sContentDbName);
    }

    @Override
    public void onError(Context context, String errorId) {
        LogManager.logInfo("Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        LogManager.logInfo("Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }
}
