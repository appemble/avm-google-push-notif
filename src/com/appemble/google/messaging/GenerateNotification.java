package com.appemble.google.messaging;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ScreenModel;

public class GenerateNotification {
    static final String STR_NOTIFICATION_ID = "notification_id";
    static int NOTIFICATION_ID = 23456;

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    public static void generateNotification(Context context, Intent serverIntent, String sMessage,
    		String sSystemDbName, String sContentDbName) {
    	if (null == serverIntent && null == sMessage)
    		return;
		if (null == Cache.context && false == Cache.initiate(context.getApplicationContext(), false)) {
			LogManager.logError("Unable to initiate the cache");
			return;
		}
		if (null == sContentDbName) {
	    	sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
	    	sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
		}
		String sScreenName = serverIntent.getStringExtra(Constants.STR_SCREEN_NAME);
		Intent notificationIntent = null;
    	Class <?> cls = null;
		if (null != sScreenName) {
	    	ScreenModel screenModel = ScreenModel.getScreenModel(sSystemDbName, sContentDbName, sScreenName);
	    	if (null == screenModel) {
				LogManager.logError("Unable to publish notification. The following screen name is not present: " + sScreenName);
				return;
	    	}
	    	cls = ScreenModel.getClass(screenModel, false);
			notificationIntent = ScreenModel.getIntent(screenModel, serverIntent.getExtras(), cls);
		}
		else {
			cls = MessagingActivity.class;
			notificationIntent = new Intent(context.getApplicationContext(), cls);
			notificationIntent.putExtras(serverIntent.getExtras());
		}
		// put the notification id...
		
		int iNotificationId = getNotificationId(serverIntent);
		// get title of the notification
		String title = null;
        if (null != serverIntent)
        	title = serverIntent.getStringExtra("title");
        if (null != title)
        	title = Cache.bootstrap.getValue("app_name");
        // get message of the notification
        String message = (null != serverIntent) ? serverIntent.getStringExtra("message") : null;
	    NotificationCompat.Builder mBuilder =
	        new NotificationCompat.Builder(context)
	        .setSmallIcon(R.drawable.ic_stat_gcm)
	        .setContentTitle(title)
	        .setContentText(message);
	    setContentNumber(context, serverIntent, mBuilder, sSystemDbName, sContentDbName); // set content number
	    mBuilder.setWhen(getNotificationGenerationTime(serverIntent));
	    setEffects(serverIntent, mBuilder); // set lights, sound, vibration
	    
	 // Creates an explicit intent for an Activity in your app

	    // The stack builder object will contain an artificial back stack for the
	    // started Activity.
	    // This ensures that navigating backward from the Activity leads out of
	    // your application to the Home screen.
	    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
	    // Adds the back stack for the Intent (but not the Intent itself)
	    stackBuilder.addParentStack(cls);
	    // Adds the Intent that starts the Activity to the top of the stack
	    stackBuilder.addNextIntent(notificationIntent);
	    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
	    mBuilder.setContentIntent(resultPendingIntent);
	    NotificationManager mNotificationManager =
	        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	    // mId allows you to update the notification later on.
	    mNotificationManager.notify(iNotificationId, mBuilder.build());
	    ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName,
	    		Messaging.ON_NOTIFICATION_RECEIVE);
	    boolean bReturn = true;
	    for (int i = 0; bReturn && null != actions && i < actions.length; i++) {
	    	Object object = Action.callAction(context, null, null, actions[i], serverIntent.getExtras());
	    	if (object instanceof Boolean)
	    		bReturn = (Boolean)object;
	    }
	    
    }

    private static int getNotificationId(Intent serverIntent) {
		String sNotificationId = serverIntent.getStringExtra(STR_NOTIFICATION_ID);
		if (null != sNotificationId) {
			try {
				return Integer.parseInt(sNotificationId);
			} catch (NumberFormatException nfe) {
				LogManager.logError("Unable to parse notification Id.");
			}
		}
		return NOTIFICATION_ID;
    }
    
    private static long getNotificationGenerationTime(Intent serverIntent) {
    	long lGenerationTime = System.currentTimeMillis();
    	String sTime = serverIntent.getStringExtra("when");
		if (null != sTime) {
			try {
				float fTime = Float.parseFloat(sTime);
				lGenerationTime = (long)fTime;
			} catch (NumberFormatException nfe) {
				LogManager.logError("Unable to parse notification generation time.");
			}
		}
		return lGenerationTime;
    }
    
    private static NotificationCompat.Builder setContentNumber(Context context, Intent serverIntent, 
    		NotificationCompat.Builder mBuilder, String sSystemDbName, String sContentDbName) {
		// get content number by raising an event ON_NOTIFICATION_CONTENT_NUMBER
    	ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName,
	    		Messaging.ON_RETRIEVE_CONTENT_NUMBER);
    	Object object = null;
    	if (null != actions && actions.length > 0)
    		object = Action.callAction(context, null, null, actions[0], serverIntent.getExtras());
    	int iContentNumber = 0;
    	if (null != object && object instanceof Integer)
    		iContentNumber = ((Integer)object).intValue();
	    if (iContentNumber > 0)
	    	mBuilder.setNumber(iContentNumber);
    	return mBuilder;
    }
    private static NotificationCompat.Builder setEffects(Intent serverIntent, NotificationCompat.Builder mBuilder) {
	    // setLights
	    int iFlags = 0;
    	boolean bLights = Utilities.parseBoolean(serverIntent.getStringExtra("lights"));
    	String sColor = serverIntent.getStringExtra("light_color");
    	if (bLights) {
    		if (null == sColor)
    			iFlags |= Notification.DEFAULT_LIGHTS;
    		else {
    			int iColor = 0; int iOnMs = 0, iOffMs = 0;
    			try {
    		    	iOnMs = Integer.parseInt(serverIntent.getStringExtra("onMs"));
    		    	iOffMs = Integer.parseInt(serverIntent.getStringExtra("offMs"));
    				iColor = Color.parseColor(sColor);
    			} catch (IllegalArgumentException iae) {
    				LogManager.logError("Unable to parse color");
    			}
    			mBuilder.setLights(iColor, iOnMs, iOffMs);
    		}
    	}
    	// setVibrate
    	String sVibratePattern = serverIntent.getStringExtra("vibrate_pattern");
    	boolean bVibrate = Utilities.parseBoolean(serverIntent.getStringExtra("vibrate"));
    	if (bVibrate) {
    		if (null == sVibratePattern) 
    			iFlags |= Notification.DEFAULT_VIBRATE;
    		else {
	    	// check if it can be parsed to long. If yes
    			String[] aVibratePattern = sVibratePattern.split(",");
    			long lVibratePattern[] = new long[aVibratePattern.length];
    			int i = 0;
    			for (String vibrateDuration : aVibratePattern) {
    				vibrateDuration = vibrateDuration.trim();
    				if (vibrateDuration.length() == 0)
    					continue;
    				try {
    					lVibratePattern[i++] = Long.parseLong(vibrateDuration);
    				} catch (IllegalArgumentException iae) {
    					LogManager.logError(iae, "Unable to parse vibrate Pattern: " + vibrateDuration);
    				}
    			}
    			mBuilder.setVibrate(lVibratePattern);
    		}	
	    }
	    // setSound
    	boolean bSound = Utilities.parseBoolean(serverIntent.getStringExtra("sound"));
    	String sSoundUri = serverIntent.getStringExtra("sound_uri");
    	if (bSound) {
    		if (null == sSoundUri)
    			iFlags |= Notification.DEFAULT_SOUND;
    		else
    			mBuilder.setSound(Uri.parse(sSoundUri));
    	}
    	// set auto-cancel - default is true
    	boolean bAutoCancel = true;
    	String sAutoCancel = serverIntent.getStringExtra("auto_cancel");
    	if (null != sAutoCancel)
    		bAutoCancel = Utilities.parseBoolean(sAutoCancel);
       	mBuilder.setAutoCancel(bAutoCancel);
    	if (iFlags > 0)
    		mBuilder.setDefaults(iFlags);
    	return mBuilder;
    }
}
