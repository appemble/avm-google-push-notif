/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.google.messaging.actions;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.actions.Action;
import com.appemble.avm.actions.ActionBase;
import com.appemble.avm.models.ActionModel;
import com.appemble.google.messaging.Messaging;

public class DEREGISTER extends ActionBase {
	// Request purchase does following

	public DEREGISTER() {} 
	
	public Object execute(final Context context, View view, View parentView, final ActionModel action,
		Bundle targetParameterValueList) {				
//		if(null == targetParameterValueList || targetParameterValueList.size() == 0)
//			return Boolean.valueOf(false);
		
		this.context = context;
		this.action = action;
		this.parentView = parentView;
		this.view = view;

		Messaging.deregister(context);
        return executeCascadingActions(true, targetParameterValueList);
	}

	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
		if (false == bExecuteResults)
			return Boolean.valueOf(bExecuteResults);
		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
    	if (object instanceof Boolean)
    		return (Boolean)object;
    	else
    		return Boolean.valueOf(false);
	} 
}