/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.appemble.google.messaging;

import static com.appemble.google.messaging.CommonUtilities.EXTRA_MESSAGE;
import static com.appemble.google.messaging.CommonUtilities.SENDER_ID;
import static com.appemble.google.messaging.CommonUtilities.REGISTER_SERVER_URL;
import static com.appemble.google.messaging.CommonUtilities.DEREGISTER_SERVER_URL;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;
import com.google.android.gcm.GCMRegistrar;

/**
 * UI to register messaging service with GCM.
 */
public class Messaging {
    AsyncTask<Void, Void, Void> mRegisterTask;
    private static Messaging _messaging = null;
    private Context context = null;
    private Bundle mAttributes;
    public static final int ON_NOTIFICATION_RECEIVE = "com.appemble.messaging.ON_NOTIFICATION_RECEIVE".hashCode();
    public static final int ON_NOTIFICATION_READ = "com.appemble.messaging.ON_NOTIFICATION_READ".hashCode();
    public static final int ON_RETRIEVE_CONTENT_NUMBER = "com.appemble.messaging.ON_RETRIEVE_CONTENT_NUMBER".hashCode();
    
    public static Messaging register(Context context, Bundle attributes) {
    	if (null == _messaging) {
    		new CommonUtilities(); // assign SERVER_URL from Strings.xml
    		_messaging = new Messaging(context);
    	}
		_messaging.mAttributes = attributes;
    	return _messaging.doRegister();
    }
    
    public static String getAttribute(String sKey) {
    	if (null == _messaging || null == sKey)
    		return null;
    	return _messaging.mAttributes.getString(sKey);
    }

    public static Bundle getAttributes() {
    	if (null == _messaging)
    		return null;
    	return _messaging.mAttributes;
    }

    private Messaging(final Context context) {
    	this.context = context;
    }
    
    private Messaging doRegister() {
        checkNotNull(REGISTER_SERVER_URL, "REGISTER_SERVER_URL");
        checkNotNull(DEREGISTER_SERVER_URL, "DEREGISTER_SERVER_URL");
        checkNotNull(SENDER_ID, "SENDER_ID");
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(context);
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(context);
//        registerReceiver(mHandleMessageReceiver,
//                new IntentFilter(DISPLAY_MESSAGE_ACTION));
        final String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("")) {
            // Automatically registers application on startup.
            GCMRegistrar.register(context, SENDER_ID);
        } else {
            // Device is already registered on GCM, check server.
            if (GCMRegistrar.isRegisteredOnServer(context)) {
                // Skips registration.
                LogManager.logDebug(Cache.bootstrap.getValue("already_registered"));
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        boolean registered =
                                ServerUtilities.register(context, mAttributes, regId);
                        // At this point all attempts to register with the app
                        // server failed, so we need to unregister the device
                        // from GCM - the app will try to register again when
                        // it is restarted. Note that GCM will send an
                        // unregistered callback upon completion, but
                        // GCMIntentService.onUnregistered() will ignore it.
                        if (!registered) {
                            GCMRegistrar.unregister(context);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }
        return this;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.options_menu, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch(item.getItemId()) {
//            /*
//             * Typically, an application registers automatically, so options
//             * below are disabled. Uncomment them if you want to manually
//             * register or unregister the device (you will also need to
//             * uncomment the equivalent options on options_menu.xml).
//             */
//            /*
//            case R.id.options_register:
//                GCMRegistrar.register(this, SENDER_ID);
//                return true;
//            case R.id.options_unregister:
//                GCMRegistrar.unregister(this);
//                return true;
//             */
//            case R.id.options_clear:
////                mDisplay.setText(null);
//                return true;
//            case R.id.options_exit:
//                finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    public static void deregister(Context context) {
    	if (null == _messaging) {
    		new CommonUtilities(); // assign SERVER_URL from Strings.xml
    		_messaging = new Messaging(context);
    	}
    	if (_messaging.mRegisterTask != null)
    		_messaging.mRegisterTask.cancel(true);
//        unregisterReceiver(mHandleMessageReceiver);
        GCMRegistrar.unregister(_messaging.context);
//            GCMRegistrar.onDestroy(_messaging.context);
//        super.onDestroy();
    }

    private void checkNotNull(Object reference, String name) {
        if (reference == null) {
            throw new NullPointerException(Cache.bootstrap.getValue("error_config") + name);
        }
    }

    public final BroadcastReceiver mHandleMessageReceiver =
            new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	Bundle extras = intent.getExtras();
        	if (null == extras)
        		return;        		
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
//            mDisplay.append(newMessage + "\n");
            Toast.makeText(context, newMessage, Toast.LENGTH_LONG).show();
    		String sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
    		String sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
    		// raise an event ON_PUSH_NOTIFICATION
        	ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName, 
        			ON_NOTIFICATION_RECEIVE);
    	    for (int i = 0; null != actions && i < actions.length; i++)
    	    	Action.callAction(context, null, null, actions[i], extras);
        }
    };
}